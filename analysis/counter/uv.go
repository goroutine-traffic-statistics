package counter

import (
	"repo.or.cz/goroutine-traffic-statistics/analysis/defs"
	"repo.or.cz/goroutine-traffic-statistics/analysis/log"
	"repo.or.cz/goroutine-traffic-statistics/analysis/rds"
	"repo.or.cz/goroutine-traffic-statistics/analysis/util"
)

func Uv(uvChan defs.UrlDataChan, storeChan defs.StorageBlockChan) {
	for urlData := range uvChan {
		rdsKey := "uv_hpll_" + getTime(urlData.Data.Time, "day")
		n, err := rds.PfAdd(rdsKey, urlData.UserID, "EX", 8400)
		if err != nil {
			log.Log.Warning("pfadd error:", err)
			continue
		}
		if n != 1 {
			log.Log.Info("uv exists")
			continue
		}
		sb := toStorageBlock(defs.CounterTypeUv, urlData.Node)
		storeChan <- sb
	}
}

func getTime(logTime, timeType string) string {
	return util.GetTime(logTime, timeType)
}
