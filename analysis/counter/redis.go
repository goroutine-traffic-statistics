package counter

import "repo.or.cz/goroutine-traffic-statistics/analysis/defs"

func toStorageBlock(types defs.CounterType, node *defs.UrlNode) *defs.StorageBlock {
	return &defs.StorageBlock{
		Type:  types,
		Model: "ZINCRBY",
		Node:  node,
	}
}
