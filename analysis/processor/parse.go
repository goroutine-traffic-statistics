package processor

import (
	"crypto/md5"
	"encoding/hex"
	"net/url"
	"strconv"
	"strings"

	"github.com/mgutz/str"
	"repo.or.cz/goroutine-traffic-statistics/analysis/defs"
)

const (
	handleDig   = " /dig?"
	handleMovie = "/movie/"
	handleList  = "/list/"
	handleHTML  = ".html"
)

func Parse(logChan defs.LogDataChan, pvChan defs.UrlDataChan, uvChan defs.UrlDataChan) error {
	for logStr := range logChan {
		data := cutLogFetchData(logStr)
		if data == nil {
			continue
		}
		hasher := md5.Sum([]byte(data.UserAgnet + data.Refer))
		uid := hex.EncodeToString(hasher[:])

		uData := &defs.UrlData{Data: data, UserID: uid, Node: formatUrl(data.Url, data.Time)}
		pvChan <- uData
		uvChan <- uData
	}
	return nil
}

func cutLogFetchData(logStr string) *defs.DigData {
	logStr = strings.TrimSpace(logStr)
	pos1 := str.IndexOf(logStr, handleDig, 0)
	if pos1 < 0 {
		return nil
	}
	pos1 += len(handleDig)
	pos2 := str.IndexOf(logStr, " HTTP/", pos1)
	d := str.Substr(logStr, pos1, pos2-pos1)

	urlInfo, err := url.Parse("http://localhost/?" + d)
	if err != nil {
		return nil
	}
	data := urlInfo.Query()
	return &defs.DigData{
		Time:      data.Get("time"),
		Url:       data.Get("url"),
		Refer:     data.Get("refer"),
		UserAgnet: data.Get("ua"),
	}
}

func formatUrl(url, time string) *defs.UrlNode {
	startMovie := str.IndexOf(url, handleMovie, 0)
	startList := str.IndexOf(url, handleList, 0)
	end := str.IndexOf(url, handleHTML, 0)
	var resourceID int
	var types defs.UrlNodeType

	if startMovie >= 0 {
		start := startMovie + len(handleMovie)
		resourceID = getResourceID(url, start, end)
		types = defs.UrlNodeTypeDetail
	} else if startList >= 0 {
		start := startList + len(handleList)
		resourceID = getResourceID(url, start, end)
		types = defs.UrlNodeTypeList
	} else {
		resourceID = 1
		types = defs.UrlNodeTypeIndex
	}

	return &defs.UrlNode{
		Type:       types,
		ResourceID: resourceID,
		Url:        url,
		Time:       time,
	}
}
func getResourceID(url string, start, end int) int {
	s := str.Substr(url, start, end-start)

	if i, err := strconv.Atoi(s); err == nil {
		return i
	}
	return 0
}
