package util

import (
	"strconv"
	"time"
)

func GetTime(logTime, timeType string) string {
	var layouter string
	switch timeType {
	case "day":
		layouter = "2006-01-02"
	case "hour":
		layouter = "2006-01-02 15"
	case "min":
		layouter = "2006-01-02 15:04"
	default:
		layouter = "2006-01-02 15:04:05"
	}
	t, _ := time.Parse(layouter, logTime)
	return strconv.FormatInt(t.Unix(), 10)
}
