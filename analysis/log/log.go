package log

import (
	"io"
	"os"

	"github.com/sirupsen/logrus"
)

var Log *logrus.Logger

func Init() {
	Log = logrus.New()
	Log.Out = os.Stdout
	Log.SetLevel(logrus.DebugLevel)
}
func SetOut(out io.Writer) {
	Log.Out = out
}
