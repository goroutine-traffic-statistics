package defs

type DigData struct {
	Time      string
	Url       string
	Refer     string
	UserAgnet string
}

type LogDataChan chan string

type UrlData struct {
	Data   *DigData
	UserID string
	Node   *UrlNode
}
type UrlDataChan chan *UrlData

type UrlNodeType string

const (
	UrlNodeTypeIndex  UrlNodeType = "index"  // 首页
	UrlNodeTypeList   UrlNodeType = "list"   // 列表
	UrlNodeTypeDetail UrlNodeType = "detail" //详情
)

type UrlNode struct {
	Type       UrlNodeType
	ResourceID int
	Url        string
	Time       string
}

type CounterType string

const (
	CounterTypePv CounterType = "pv"
	CounterTypeUv CounterType = "uv"
)

type StorageBlock struct {
	Type  CounterType
	Model string
	Node  *UrlNode
}
type StorageBlockChan chan *StorageBlock

type CmdParam struct {
	LogFile   string
	WorkerNum int
}
