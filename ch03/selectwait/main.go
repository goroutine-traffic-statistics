package main

import (
	"fmt"
	"time"
)

func foo(ch chan string) {
	for i := 0; i < 5; i++ {
		ch <- fmt.Sprintf("foo #%d", i)
		time.Sleep(time.Second * 1)
	}
	//close(ch)
}

func bar(ch chan int) {
	for i := 0; i < 9; i++ {
		ch <- i
		time.Sleep(time.Second * 5)
	}
	//close(ch)
}

func main() {
	ch1 := make(chan string, 3)
	ch2 := make(chan int, 5)

	for i := 0; i < 10; i++ {
		go foo(ch1)
		go bar(ch2)
	}

	for {
		select {
		case str, ok := <-ch1:
			fmt.Println("ch1:", str, ok)
		case i, ok := <-ch2:
			fmt.Println("ch2:", i, ok)
		}
	}
}
