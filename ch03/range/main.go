package main

import (
	"fmt"
	"time"
)

func foo(msg chan<- string) {
	for i := 0; i < 4; i++ {
		msg <- fmt.Sprintf("你好，协程%d", i+1)
	}
}

func bar(msg chan string) {
	time.Sleep(time.Second)

	str := <-msg + "🌞"
	msg <- str

	close(msg)
}
func main() {
	msg := make(chan string, 3)
	go foo(msg)
	go bar(msg)

	time.Sleep(time.Second * 3)

	for str := range msg {
		fmt.Println(str)
	}

	fmt.Println("你好，世界")
}
