#include <stdio.h>
#include <unistd.h>

int main(int argc, char **argv) {
    printf("-- 程序开始 --\n");

    int counter = 0;
    pid_t pid = fork();

    if(pid == 0) {
        // 子进程
        int i = 0;
        for(;i<5;i++) {
            printf("子进程：counter=%d\n", ++counter);
        }
    } else if (pid > 0) {
        // 父进程
        int j = 0;
        for(; j<5;j++) {
            printf("父进程：counter=%d\n", ++counter);
        }
    } else {
        printf("fork()失败！\n");
        return 1;
    }
    printf("-- 程序结束 --\n");
    return 0;
}
